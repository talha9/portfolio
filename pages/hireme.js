import Layout from '../components/Layout';

const Hireme = () => (
    <Layout title="Hire Me"> 
        <p>You can Hire me as a full time resource.</p>
    </Layout>
);

export default Hireme;